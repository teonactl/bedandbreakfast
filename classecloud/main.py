from flask import Flask, render_template

app = Flask(__name__)

@app.route('/about')
def hello_world():
    return render_template('about.html')




if __name__ == '__main__':
    app.run()
